﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoiseScript : MonoBehaviour
{

    int NoiseTime;
    
    public AudioClip noiseSound;
    AudioSource audioSource;

    [SerializeField]
    private GameObject NoiseEffect;

    [SerializeField]
    private GameObject BackGroundNoise;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        //NoiseEffect.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        NoiseTime = TextController.GetCurrentLine();
        if (NoiseTime == 4)
        {
            NoiseEffect.SetActive(true);
            BackGroundNoise.SetActive(false);
            Invoke("NoiseOff",0.2f);
            audioSource.PlayOneShot(noiseSound);

        }
    }

    public void NoiseOff()
    {
        NoiseEffect.SetActive(false);
        BackGroundNoise.SetActive(true);
        audioSource.Stop();
    }
}
