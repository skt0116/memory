﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeScene1 : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Invoke("ChangeScene01", 100.0f);

        //Invoke("ChangeScene01", 10.0f);

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void ChangeScene01()
    {
        FadeManager.Instance.LoadScene("Result", 1.0f);
    }
}
