﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultScore : MonoBehaviour
{

    public Text ScoreText;
    public Text maxComboText;
    public Text perfectText;
    public Text goodText;
    public Text badText;
    public Text missText;
    int _score;
    int _comboCnt;
    int _perfect;
    int _good;
    int _bad;
    int _miss;
    string _beforeScene;

    public GameObject RankS;
    public GameObject RankA;
    public GameObject RankB;
    public GameObject RankC;



    // Start is called before the first frame update
    void Start()
    {
        _score = GameController.getscore();
        ScoreText.text = string.Format("Score : {0}", _score);

        _comboCnt = GameController.getmaxCombo();
        maxComboText.text = string.Format("Combo : {0}", _comboCnt);

        _perfect = GameController.getPerfect();
        perfectText.text = string.Format("Perfect : {0}", _perfect);

        _good = GameController.getGood();
        goodText.text = string.Format("Good : {0}", _good);

        _bad = GameController.getBad();
        badText.text = string.Format("Bad : {0}", _bad);

        _miss = GameController.getMiss();
        missText.text = string.Format("Miss : {0}", _miss);

        _beforeScene = GameController.getSceneName();

    }

    // Update is called once per frame
    void Update()
    {
        if(_miss == 0 && _bad == 0)
        {
            RankS.SetActive(true);
        }
        else if(_comboCnt >= 100 && _miss != 0)
        {
            RankA.SetActive(true);
        }
        else if (_comboCnt < 100 && _comboCnt >= 50 && _miss != 0)
        {
            RankB.SetActive(true);
        }
        else
        {
            RankC.SetActive(true);
        }

    }
}