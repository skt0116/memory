﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ResultScript : MonoBehaviour
{
    string _beforeScene;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        _beforeScene = GameController.getSceneName();
    }
    //シーン切り替え
    //Gameシーンに切り替え
    public void RETRYButton()
    {
        SceneManager.LoadScene(_beforeScene);
    }
    //TITLEシーンに切り替え
    public void TITLEButton()
    {
        FadeManager.Instance.LoadScene("Title",1.0f);
    }
    //次のシーンに切り替え
    public void NEXTButton()
    {
        FadeManager.Instance.LoadScene("GameOver",1.0f);
    }

}
