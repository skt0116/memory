﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Result : MonoBehaviour
{

    [SerializeField]
    private GameObject target;//オブジェクト名を作成

    // Start is called before the first frame update
    void Start()
    {
        Invoke("result", 3.0f);//3秒後に！(ボタン)
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void result()
    {
        target.SetActive(true);//ボタンを表示
    }
//--------------------------------------------------------------------
//ボタンの行き先
/*
    public void RetryButton()
    {
        SceneManager.LoadScene("Game");
    }
    */
}
