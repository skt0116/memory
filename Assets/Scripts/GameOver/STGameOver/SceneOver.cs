﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class SceneOver : MonoBehaviour
{
    string _beforeScene;
    // Start is called before the first frame update
    void Start()
    {
        _beforeScene = GameController.getSceneName();   
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void RetryButton()
    {
        SceneManager.LoadScene(_beforeScene);
    }
    public void TitleButton()
    {
        FadeManager.Instance.LoadScene("Title",1.0f);
    }
}
