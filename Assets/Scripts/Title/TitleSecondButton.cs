﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TitleSecondButton : MonoBehaviour
{
    public GameObject TouchToStartButton;//スタートボタン

    public GameObject button;//スタートボタン

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void StartGame()
    {
        TouchToStartButton.SetActive(false);//スタートボタンを非表示
        button.SetActive(true);//スタートボタンを非表示
    }
}