﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TitleSceneManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    //シーン切り替え
    //Gameシーンに切り替え
    public void STORYButton()
    {
        FadeManager.Instance.LoadScene("StorySelect", 1.0f);
    }
    //TITLEシーンに切り替え
    public void MUSICButton()
    {
        FadeManager.Instance.LoadScene("MusicSelect", 1.0f);
    }
    //次のシーンに切り替え
    public void QUITButton()
    {
        UnityEngine.Application.Quit();
    }
}
