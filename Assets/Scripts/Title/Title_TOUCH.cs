﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Title_TOUCH : MonoBehaviour
{
    public float interval = 0.1f;
    
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("Blink");

        //コールチンはTime.timeScaleに影響されるため、Time.timeScale = 0の時コールチン内が永久に再開されない

        Time.timeScale = 1; //通常再生
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    IEnumerator Blink()
    {
        while (true)
        {
            var renderComponent = GetComponent<Renderer>();
            renderComponent.enabled = !renderComponent.enabled;
            yield return new WaitForSeconds(interval);
        }
    }
}
