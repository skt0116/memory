﻿using UnityEngine;
using System.Collections;

public class NotesTimingMaker : MonoBehaviour
{
    private AudioSource _audioSource;//音声ファイル
    private float _startTime = 0;//始まる時間
    private CSVWriter _CSVWriter;//CSVに書き込むプログラム

    private bool _isPlaying = false;//プレイ中か否か
    public GameObject startButton;//スタートボタン

    void Start()
    {
        _audioSource = GameObject.Find("GameMusic").GetComponent<AudioSource>();//ゲームミュージックを取得
        _CSVWriter = GameObject.Find("CSVWriter").GetComponent<CSVWriter>();//CSVに書き込む
    }

    void Update()
    {
        if (_isPlaying)
        {
            DetectKeys();
        }
    }

    public void StartMusic()
    {
        startButton.SetActive(false);//スタートボタンを非表示
        _audioSource.Play();//音楽を再生
        _startTime = Time.time;//開始時間
        _isPlaying = true;//プレイ中
    }

    void DetectKeys()
    {
        //キーを押した瞬間ラインナンバーのところにノーツを生成
        if (Input.GetKeyDown(KeyCode.D))
        {
            WriteNotesTiming(0);
        }

        if (Input.GetKeyDown(KeyCode.F))
        {
            WriteNotesTiming(1);
        }

        if (Input.GetKeyDown(KeyCode.J))
        {
            WriteNotesTiming(2);
        }

        if (Input.GetKeyDown(KeyCode.K))
        {
            WriteNotesTiming(3);
        }
    }

    void WriteNotesTiming(int num)
    {
        Debug.Log(GetTiming());
        _CSVWriter.WriteCSV(GetTiming().ToString() + "," + num.ToString());//CSVWriterに投げる
    }

    float GetTiming()
    {
        return Time.time - _startTime;
    }



}

