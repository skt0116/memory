﻿using UnityEngine;
using System.Collections;
using System.IO;  // <- ここに注意

public class CSVWriter : MonoBehaviour
{

    public string fileName; // 保存するファイル名

    
      /*void Start () {
          WriteCSV ("Hello,World");
      }*/   

    // CSVに書き込む処理
    public void WriteCSV(string txt)
    {
        StreamWriter streamWriter;
        FileInfo fileInfo;
        fileInfo = new FileInfo(Application.dataPath + "/" + fileName + ".csv");//CSVファイル生成
        streamWriter = fileInfo.AppendText();//数値を書き込む
        streamWriter.WriteLine(txt);//ラインナンバーを書き込む
        streamWriter.Flush();//保存
        streamWriter.Close();//閉じる
    }
}