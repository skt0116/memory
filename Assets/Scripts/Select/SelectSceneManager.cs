﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class SelectSceneManager : MonoBehaviour
{
    // Start is called before the first frame update
    private static string _nowScene;
    public AudioClip Mortals;
    AudioSource audioSourceM;

    public AudioClip TurnItUp;
    AudioSource audioSourceT;


    int FSC = 0;
    int SSC = 0;

    void Start()
    {
        _nowScene = SceneManager.GetActiveScene().name;
        audioSourceM = GetComponent<AudioSource>();
        audioSourceT = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    //音楽選択
    public void Button1()
    {
        audioSourceM.Stop();
        SSC = 0;
        FSC++;//FirstStoryCnt
        if (_nowScene == "StorySelect")
        {
            if (FSC == 1)
            {
                audioSourceM.PlayOneShot(Mortals);
            }
            if (FSC == 2)
            {
                FadeManager.Instance.LoadScene("Story", 1.5f);
                FSC = 0;
                audioSourceM.Stop();
            }
        }
        else if (_nowScene == "MusicSelect")
        {
            if (FSC == 1)
            {
                audioSourceM.PlayOneShot(Mortals);
            }
            if (FSC == 2)
            {
                SceneManager.LoadScene("MU_Mortals");
                FSC = 0;
                audioSourceM.Stop();
            }
        }
    }


    public void Button2()
    {
        audioSourceT.Stop();
        FSC = 0;
        SSC++;//SecondStoryCnt
        if (_nowScene == "StorySelect")
        {
            if (SSC == 1)
            {
                audioSourceT.PlayOneShot(TurnItUp);
            }
            if (SSC == 2)
            {
                FadeManager.Instance.LoadScene("Story", 1.5f);
                SSC = 0;
                audioSourceT.Stop();
            }
        }
        else if (_nowScene == "MusicSelect")
        {
            if (SSC == 1)
            {
                audioSourceT.PlayOneShot(TurnItUp);
            }
            if (SSC == 2)
            {
                SceneManager.LoadScene("MU_TurnItUp");
                SSC = 0;
                audioSourceT.Stop();
            }
        }   
    }
}
