﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SelectStoryManager : MonoBehaviour
{
    public AudioClip GameMusic;
    AudioSource audioSource;
    int FSC = 0;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
//------------------------------------------------------------
//ストリーの選択
    public void FirstStory()//1
    {
        FSC++;//FirstStoryCnt
        if(FSC == 1)
        {
            audioSource.PlayOneShot(GameMusic);
        }
        if(FSC == 2)
        {
            FadeManager.Instance.LoadScene("Story", 1.5f);
            FSC = 0;
            audioSource.Stop();
        }
       
    }
    
//-------------------------------------------------------------
    //セレクトからタイトルへ
    public void BackButton()
    {
        FadeManager.Instance.LoadScene("Title", 0.5f);
    }

}
