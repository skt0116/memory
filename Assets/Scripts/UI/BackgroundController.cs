﻿using UnityEngine;
using System.Collections;

public class BackgroundController : MonoBehaviour
{
    void FixedUpdate()
    {
        transform.Translate(-0.09f, 0, 0);
        if (transform.position.x < -20.0f)
        {
            transform.position = new Vector3(20.0f, 0, 0);
        }
    }
}