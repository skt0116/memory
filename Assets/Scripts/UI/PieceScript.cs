﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PieceScript : MonoBehaviour
{
    int Score;
    //空のピース
    public  GameObject Ps1;//1つ目のピース
    public  GameObject Ps2;//2つ目のピース
    public  GameObject Ps3;//3つ目のピース
    //ピースの獲得済み
    public  GameObject Ps_1;//1つ目のピース
    public  GameObject Ps_2;//2つ目のピース
    public  GameObject Ps_3;//3つ目のピース

    // Start is called before the first frame update
    void Start()
    {

    }
    public  void Initialize()
    {
        /*
        Ps_1.SetActive(false);
        Ps1.SetActive(true);
        Ps_2.SetActive(false);
        Ps2.SetActive(true);
        Ps_3.SetActive(false);
        Ps3.SetActive(true);
        */
    }
    // Update is called once per frame
    void Update()
    {
        Score = GameController.getscore();

        if (Score >= 1000)
        {
            Ps_1.SetActive(true);
            Ps1.SetActive(false);
        }
        else
        {
            Ps_1.SetActive(false);
            Ps1.SetActive(true);
        }
        if (Score >= 5000)
        {
            Ps_2.SetActive(true);
            Ps2.SetActive(false);
        }
        else
        {
            Ps_2.SetActive(false);
            Ps2.SetActive(true);
        }
        if (Score >= 10000)
        {
            Ps_3.SetActive(true);
            Ps3.SetActive(false);
        }
        else
        {
            Ps_3.SetActive(false);
            Ps3.SetActive(true);
        }
    }
   
}
