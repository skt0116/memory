﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonScripts : MonoBehaviour
{
    public bool push;
    // Start is called before the first frame update
    void Start()
    {
       push = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void PushButton()
    {
        push = true;
    }
    public void LeaveButton()
    {
        push = false;
    }
}
