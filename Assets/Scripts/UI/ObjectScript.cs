﻿using UnityEngine;
using System.Collections;

public class ObjectScript : MonoBehaviour
{
    public int lineNum;//ラインのナンバー
    private ObstacleScript _obstacleScript;//GameController
    private bool isInLine = false;//キーを押せる状況か否か
    private string judgeName;//判定の名前

    public float _debug_time;//offsettimeの時間を調べる
    private bool _debug_time_drawed;

    private Vector3 touchStartPos;
    private Vector3 touchEndPos;

    void Start()
    {
        _obstacleScript = GameObject.Find("ObstacleController").GetComponent<ObstacleScript>();//GameControllerを取得
        _debug_time = Time.time;//OffSetTimeに必要な時間を取得

    }


    void Update()
    {
        //this.transform.position += Vector3.left * 10 * Time.deltaTime;
        /*
        left*15と<-10.0fだと大体タイミングが合う
        */

        var RigidBody = this.GetComponent<Rigidbody2D>();//RigidBodyを取得
        RigidBody.MovePosition(RigidBody.position + Vector2.left * 5 * Time.fixedDeltaTime);//ノーツが流れるスピード

        if (this.transform.position.x < -10.0f)//どこまで流れるか
        {
            //Debug.Log("false");
            Destroy(this.gameObject);//ノーツプレハブを消す
        }

        //Debug.Log(isInLine);
        if (isInLine)//判定内なら
        {
            
        }

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        isInLine = true;//判定内
        //Debug.Log("OnTriggerEnter2D: " + other.gameObject.name);
        judgeName = other.gameObject.name;//判定の名前を取得


        // debug ノーツ生成からパーフェクトまでの時間

        if (judgeName == "New_Player_0" && !_debug_time_drawed)
        {
            _debug_time_drawed = true;
            Debug.Log("TIME : " + (Time.time - _debug_time));
        }

    }

    void OnTriggerExit2D(Collider2D other)
    {
        isInLine = false;//判定外

        // Debug.Log("OnTriggerExit2D: " + other.gameObject.name);
    }
}