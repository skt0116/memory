﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;
using UnityEngine.UI;

public class ObstacleScript : MonoBehaviour
{

    public GameObject[] notes;//ノーツ
    //private float[] _timing;//タイミング
    //private int[] _lineNum;//ラインナンバー

    //public string filePass;//ファイルパス
    //private int _notesCount = 0;//ノーツカウント

    //public AudioSource _audioSource;//音声
    //private float _startTime = 0;//スタート時間

    //public float timeOffset = -1;//ノーツと音楽の調整

    //private bool _isPlaying = false;//プレイ中
    //public GameObject startButton;//スタートボタン

    /*
    void Start()
    {
        //_audioSource = GameObject.Find("GameMusic").GetComponent<AudioSource>();//ゲームミュージック取得
        _timing = new float[1024];//タイミング
        _lineNum = new int[1024];//ラインナンバー
        LoadCSV();//CSVを取得し譜面を流す。
        Time.timeScale = 0;//停止状態
    }

    void Update()
    {
        if (_isPlaying)
        {
            CheckNextNotes();//プレイ中ノーツをチェック
        }
    }
    public void StartGame()
    {
        startButton.SetActive(false);//スタートボタンを非表示
        //_startTime = Time.time;//スタート時の時間取得
        //_audioSource.Play();//音楽再生
        _isPlaying = true;//プレイ中
        Time.timeScale = 1f;//通常プレイ
    }

    void CheckNextNotes()
    {
        while (_timing[_notesCount] + timeOffset < GetMusicTime() && _timing[_notesCount] != 0)//ノーツのタイミング
        {
            SpawnNotes(_lineNum[_notesCount]);//ノーツ生成
            _notesCount++;//次のノーツ
        }
    }
    */
    public void SpawnNotes(int LaneNum)
    {
        //Debug.Log("LaneNum:" + LaneNum);
        GameObject Lane;//オブジェクトが流れるレーン
        switch (LaneNum)
        {
            case -1:
                return;
                break;
            case 0:
                Lane = GameObject.Find("WoodObjectLane");
                break;
            case 1:
                Lane = GameObject.Find("RockObjectLane");
                break;
            case 2:
                Lane = GameObject.Find("BoxObjectLane");
                break;
            case 3:
                Lane = GameObject.Find("BranchObjectLane");
                break;
            default:
                Lane = GameObject.Find("WoodObjectLane");
                break;
        }
        var instance = Instantiate(notes[LaneNum],
            new Vector3(1910.0f, 0.0f, 0),
            Quaternion.identity);
        instance.transform.SetParent(Lane.transform, false);
    }
    /*
    void LoadCSV()
    {
        int i = 0, j;
        TextAsset csv = Resources.Load(filePass) as TextAsset;//CSVの場所
        StringReader reader = new StringReader(csv.text);
        while (reader.Peek() > -1)//セル毎に読み取っていく
        {
            string line = reader.ReadLine();
            string[] values = line.Split(',');

            for (j = 0; j < values.Length; j++)
            {
                _timing[i] = float.Parse(values[0]);
                _lineNum[i] = int.Parse(values[1]);
            }
            i++;
        }
    }

    float GetMusicTime()
    {
        return Time.time - _startTime;//音楽の時間を取得
    }
    */
}