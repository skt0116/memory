﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyScript : MonoBehaviour
{
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
敵の座標　X(-6.5)、Y(-0.5)
*///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //変数定義
    //public float scroll = 2.0f;
    //float direction = 0.0f;
    int distanceCnt = 0;//ミスしてカウントされる器
    private int Suport;
    int postDistanceCnt = 0;//ミスしてカウントされる器
    Rigidbody2D rb2d;
    private const float moveDistance = 1.0f; // 移動距離
    private const float startPositionX = -6.5f; // 敵の初期位置
    private float elapsedTime = 0.0f;   // 経過時間
    private const float requiredTime = 1.0f; // 移動にかける時間

    int missCnt;
    int missSuportCnt;
    bool changeScene;
    // Use this for initialization
    void Start()
    {
        //コンポーネント読み込み
        rb2d = GetComponent<Rigidbody2D>();
        changeScene = false;
        
    }


    // Update is called once per frame
    void Update()
    {
        /*
        if (Input.GetKeyDown(KeyCode.P))       //右に動く
        {
            distanceCnt++;
        }
        if (Input.GetKeyDown(KeyCode.O))       //左に動く
        {
            distanceCnt--;
        }
        */
        //missCnt = NoteScript.getMissCount();
        //Debug.Log("miss:"+missCnt);
        //Debug.Log("sup:"+ missSuportCnt);
        //Debug.Log("dis:" + distanceCnt);
        missCnt = NoteScript.getMissCountYade();
        missSuportCnt = NoteScript.getMissSuportCount();

        distanceCnt = missCnt - missSuportCnt;
        //五回目でゲームオーバー
        if (distanceCnt >= 5)
         {
            if(!changeScene)
            {
                changeScene = true;
                Invoke("GameOver",1.5f);
                
            }
        }


        if (distanceCnt != postDistanceCnt)
        {
            // 経過時間
            elapsedTime += Time.deltaTime;

            // 線形補完
            var lerpX = Mathf.Lerp(
                //敵の初期位置＋ミスしてカウントされる器×移動距離
                startPositionX + postDistanceCnt * moveDistance,
                //敵の初期位置＋
                startPositionX + distanceCnt * moveDistance,
                elapsedTime / (requiredTime * Mathf.Abs(distanceCnt - postDistanceCnt))
            );

            // 経過時間が必要時間を上回ったら移動終了
            if(elapsedTime >= (requiredTime * Mathf.Abs(distanceCnt - postDistanceCnt)))
            {
                lerpX = startPositionX + distanceCnt * moveDistance; // 移動先を終端にしておく
                postDistanceCnt = distanceCnt;
                elapsedTime = 0.0f;
            }
            //Debug.Log(elapsedTime + " : " + lerpX);

            transform.position = new Vector2(lerpX, transform.position.y);
        }

        //キャラのy軸のdirection方向にscrollの力をかける
        //rb2d.velocity = new Vector2(scroll * direction, rb2d.velocity.y);
        //Distance(int distance);
    }
    void GameOver()
    {
        FadeManager.Instance.LoadScene("GameOver", 1.0f);
        Invoke("Initialize",1.0f);
        changeScene = false;
    }
    void Initialize()
    {
        NoteScript.Initialize();
    }
}