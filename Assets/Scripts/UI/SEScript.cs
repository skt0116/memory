﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SEScript : MonoBehaviour
{
    public AudioClip sound1;
    AudioSource audioSource;

    void Start()
    {
        //Componentを取得
        audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        // 左
        if (NoteScript.push)
        {
            //音(sound1)を鳴らす
            audioSource.PlayOneShot(sound1);
            NoteScript.push = false;
        }
    }
}
