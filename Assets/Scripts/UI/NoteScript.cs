﻿using UnityEngine;
using System.Collections;

public class NoteScript : MonoBehaviour
{
    public int lineNum;//ラインのナンバー
    private GameController _gameController;//GameController
    private bool isInLine = false;//キーを押せる状況か否か
    private KeyCode _lineKey;//ラインごとのキーコード
    private string judgeName;//判定の名前
    private int combo;//コンボ
    public static int miss;
    private static int missSuport;
    private static int missCnt;
    public int AnimationCount;
    public int ObjectLaneNum;

    private GameObject _player;

    public float _debug_time;//offsettimeの時間を調べる
    private bool _debug_time_drawed;
    public static bool push = false;

    private Vector3 touchStartPos;//タッチ始め
    private Vector3 touchEndPos;//タッチ終わり

    void Start()
    {
        _gameController = GameObject.Find("GameController").GetComponent<GameController>();//GameControllerを取得
        _lineKey = GameUtil.GetKeyCodeByLineNum(lineNum);//ライン毎に押せるキーを取得
        _debug_time = Time.time;//OffSetTimeに必要な時間を取得
        AnimationCount = 0;

        _player = GameObject.Find("New_Player_0");
    }


    void Update()
    {
        //this.transform.position += Vector3.left * 10 * Time.deltaTime;
        /*
        left*15と<-10.0fだと大体タイミングが合う
        */
        
        var RigidBody = this.GetComponent<Rigidbody2D>();//RigidBodyを取得
        RigidBody.MovePosition(RigidBody.position + Vector2.left * 5 * Time.fixedDeltaTime);//ノーツが流れるスピード

        if (this.transform.position.x < -10.0f)//どこまで流れるか
        {
            //Debug.Log("false");
            Destroy(this.gameObject);//ノーツプレハブを消す
        }

        //Debug.Log(isInLine);
        if (isInLine)//判定内なら
        {
            CheckInput(_lineKey);//キーを取得
            if (GameController.isBad)//Badを踏んだら
            {
                missCnt++;//missCntをインクリメント
                GameController.isBad = false;//Badをfalse
            }
        }
        


        Flick();
        

       
    }
    
    public static int getMissSuportCount()
    {
        return missSuport;
    }
    public static int getMissCountYade()
    {
        return missCnt;
    }
    public static void Initialize()
    {
        miss = 0;
        missCnt = 0;
        missSuport = 0;
        GameController.isBad = false;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        isInLine = true;//判定内
        //Debug.Log("OnTriggerEnter2D: " + other.gameObject.name);
        judgeName = other.gameObject.name;//判定の名前を取得
        if (isInLine)
        {
            //if (missCnt > 5)Initialize();

            if (judgeName == "Miss")//Missならコンボを初期化しノーツプレハブを消す
            {
                combo = 0;//コンボリセット
                miss++;
                missCnt++;

                _gameController.MissTiming(combo, miss);//MissTimingにコンボの値を投げる
                _gameController.JudgeMiss();
                Destroy(this.gameObject);//ノーツプレハブを消す
            }
           
            if (judgeName == "Perfect" || judgeName == "Good")
            {
                combo = GameController.getNowCombo();
                if(combo > 10 && missCnt > missSuport)//
                {
                    missSuport++;
                }
            }
        }
        

        // debug ノーツ生成からパーフェクトまでの時間

        if (judgeName == "New_Player_0" && !_debug_time_drawed)
        {
            _debug_time_drawed = true;
            Debug.Log("TIME : " + (Time.time - _debug_time));
        }
        
    }

    void OnTriggerExit2D(Collider2D other)
    {
        isInLine = false;//判定外
        
        // Debug.Log("OnTriggerExit2D: " + other.gameObject.name);
    }
    
    void CheckInput(KeyCode key)
    {
        if (Input.GetKeyDown(key))//キーを押した瞬間
        {
            push = true;
            _gameController.GoodTimingFunc(lineNum,judgeName);//ラインナンバーと判定をGoodTimingFuncに投げる
            Destroy(this.gameObject);//ノーツプレハブを消す
        }
        /*
        if(direction == 8)
        {
            _gameController.GoodTimingFunc(lineNum,judgeName);
            Destroy(this.gameObject);
        }
        if (direction == 6)
        {
            _gameController.GoodTimingFunc(lineNum, judgeName);
            Destroy(this.gameObject);
        }
        */
    }
    void Flick()
    {       
        GameObject WButton = GameObject.Find("WhiteButton");
        GameObject BButton = GameObject.Find("BlackButton");
        
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            


            if (WButton.GetComponent<ButtonScripts>().push || BButton.GetComponent<ButtonScripts>().push)
            {
                touchStartPos = new Vector3(Input.mousePosition.x,
                                            Input.mousePosition.y,
                                            Input.mousePosition.z);
                
            }
        }
        
        if(Input.GetKey(KeyCode.Mouse0))
        {
            if (WButton.GetComponent<ButtonScripts>().push || BButton.GetComponent<ButtonScripts>().push)
            { 
                touchEndPos = new Vector3(Input.mousePosition.x,
                                      Input.mousePosition.y,
                                      Input.mousePosition.z);
                GetDirection();
            }
        }
        /*if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            touchEndPos = new Vector3(Input.mousePosition.x,
                                      Input.mousePosition.y,
                                      Input.mousePosition.z);
            
        }*/
    }
    public void GetDirection()
    {
        float directionX = touchEndPos.x - touchStartPos.x;
        float directionY = touchEndPos.y - touchStartPos.y;
        string Direction = null;
        



        if (Mathf.Abs(directionX)<Mathf.Abs(directionY)){
            if (1 < directionY){
                //上向きにフリック
                Direction = "up";
            }else if (-1 > directionY){
                //下向きのフリック
                Direction = "down";
            }

        }else{
            //タッチを検出
            Direction = "touch";
        }
         if (isInLine) { 

            switch (Direction)
            {
                case "up":
                    //上フリックされた時の処理
                    if(lineNum == 0 || lineNum == 2)
                    {
                        _gameController.GoodTimingFunc(lineNum, judgeName);//ラインナンバーと判定をGoodTimingFuncに投げる
                        push = true;
                        Destroy(this.gameObject);//ノーツプレハブを消す
                        if (ObjectLaneNum == 1 || ObjectLaneNum == 2)//ジャンプモーション条件
                        {
                             _player.GetComponent<Transition>().Jump();
                        }
                       
                    }
                    break;

                case "down":
                    //下フリックされた時の処理
                    if (lineNum == 1 || lineNum == 3) 
                    {
                        _gameController.GoodTimingFunc(lineNum, judgeName);//ラインナンバーと判定をGoodTimingFuncに投げる
                        push = true;
                        Destroy(this.gameObject);//ノーツプレハブを消す
                    }
                    break;
                case "touch":
                    //タッチされた時の処理
                 break;
            }
         }
    }

    
}
