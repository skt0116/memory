﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{

    public GameObject[] notes;//ノーツ
    private float[] _timing;//タイミング
    private int[] _lineNum;//ラインナンバー
    private int[] _objLineNum;//オブジェクトのラインナンバー

    public string filePass;//ファイルパス
    private int _notesCount = 0;//ノーツカウント

    public AudioSource _audioSource;//音声
    private float _startTime = 0;//スタート時間

    public float timeOffset = -1;//ノーツと音楽の調整

    private bool _isPlaying = false;//プレイ中
    public GameObject startButton;//スタートボタン

    public Text scoreText;//スコアテキスト
    public static int _score = 0;//スコア

    public Text comboText;//コンボテキスト
    public Text maxComboText;//マックスコンボテキスト
    public static int _combo = 0;//コンボ
    public static int _comboCnt = 0;//マックスコンボ

    public GameObject PerfectUI;
    public GameObject GoodUI;
    public GameObject BadUI;
    public GameObject MissUI;

    public static string _nowScene;

    public static int _perfect = 0;
    public static int _good = 0;
    public static int _bad = 0;
    public static int _miss = 0;

    public static bool isBad = false;

    int per = 0;
    int good = 0;

    //int judgeMiss = 0;
    int missJudge = 0;
    //スコア取得(継承)
    public static string getSceneName()
    {
        return _nowScene;
    }
    public static int getscore()
    {
        return _score;
    }
    //最大コンボ取得(継承)
    public static int getmaxCombo()
    {
        return _comboCnt;
    }
    //perfect取得(継承)
    public static int getPerfect()
    {
        return _perfect;
    }
    //good取得(継承)
    public static int getGood()
    {
        return _good;
    }
    //bad取得(継承)
    public static int getBad()
    {
        return _bad;
    }
    //miss取得(継承)
    public static int getMiss()
    {
        return _miss;
    }
    public static int getNowCombo()
    {
        return _combo;
    }

    void Start()
    {
        _audioSource = GameObject.Find("GameMusic").GetComponent<AudioSource>();//ゲームミュージック取得
        _timing = new float[1024];//タイミング
        _lineNum = new int[1024];//ラインナンバー
        _objLineNum = new int[1024];//ラインナンバー
        LoadCSV();//CSVを取得し譜面を流す。
        Time.timeScale = 0;//停止状態
        _nowScene = SceneManager.GetActiveScene().name;
    }

    void Update()
    {
        //Debug.Log("現在のシーン:" + _nowScene);
        if (_isPlaying)
        {
            CheckNextNotes();//プレイ中ノーツをチェック
            scoreText.text = _score.ToString();//スコアをStringに変換
            comboText.text = _combo.ToString();//コンボをStringに変換
            maxComboText.text = _comboCnt.ToString();//マックスコンボをStringに変換
        }
        if (_combo < 10){ good = 100; per = 300; }
        else if (_combo >= 10 && _combo < 20) { good = 110; per = 330; }
        else if (_combo >= 20 && _combo < 30) { good = 120; per = 360; }
        else if (_combo >= 30 && _combo < 40) { good = 130; per = 390; }
        else if (_combo >= 40 && _combo < 50) { good = 140; per = 420; }
        else if (_combo >= 50 && _combo < 60) { good = 150; per = 450; }
        else if (_combo >= 60 && _combo < 70) { good = 160; per = 480; }
        else if (_combo >= 70 && _combo < 80) { good = 170; per = 510; }
        else if (_combo >= 80 && _combo < 90) { good = 180; per = 540; }
        else if (_combo >= 90 && _combo < 100) { good = 190; per = 570; }
        else if (_combo >= 100 && _combo < 110) { good = 200; per = 600; }
        else if (_combo >= 110 && _combo < 120) { good = 210; per = 630; }
        else if (_combo >= 120 && _combo < 130) { good = 220; per = 690; }
    }
    public void StartGame()
    {
        _perfect = 0;
        _good = 0;
        _bad = 0;
        _miss = 0;
        _score = 0;//スコア
        _combo = 0;//現在のコンボ
        _comboCnt = 0;//マックスコンボ
        NoteScript.Initialize();
        //PieceScript.Initialize();
        startButton.SetActive(false);//スタートボタンを非表示
        _startTime = Time.time;//スタート時の時間取得
        _audioSource.Play();//音楽再生
        _isPlaying = true;//プレイ中
        Time.timeScale = 1f;//通常プレイ
    }

    void CheckNextNotes()
    {
        while (_timing[_notesCount] + timeOffset < GetMusicTime() && _timing[_notesCount] != 0)//ノーツのタイミング
        {
            SpawnNotes(_lineNum[_notesCount]);//ノーツ生成
            _notesCount++;//次のノーツ
        }
    }

    void SpawnNotes(int LaneNum)
    {
        GameObject Lane;//ノーツが流れるレーン
        switch(LaneNum)
        {
            case 0:
                Lane = GameObject.Find("WhiteLane");
                break;
            case 1:
                Lane = GameObject.Find("WhiteLane2");
                break;
            case 2:
                Lane = GameObject.Find("BlackLane");
                break;
            case 3:
                Lane = GameObject.Find("BlackLane2");
                break;
            default:
                Lane = GameObject.Find("WhiteLane");
                break;
        }
        var instance = Instantiate(notes[LaneNum],
            new Vector3(960.0f, 0.0f, 0),
            Quaternion.identity);
        instance.transform.SetParent(Lane.transform, false);
        instance.GetComponent<NoteScript>().ObjectLaneNum = _objLineNum[_notesCount];//プレイヤージャンプのための特定のオブジェクト

        GameObject ObstacleController = GameObject.Find("ObstacleController");
        ObstacleController.GetComponent<ObstacleScript>().SpawnNotes(_objLineNum[_notesCount]);
    }

    void LoadCSV()
    {
        int i = 0, j;
        TextAsset csv = Resources.Load(filePass) as TextAsset;//CSVの場所
        StringReader reader = new StringReader(csv.text);
        while (reader.Peek() > -1)//セル毎に読み取っていく
        {
            string line = reader.ReadLine();
            string[] values = line.Split(',');

            //Debug.Log("line:" + line);
            //Debug.Log("values:" + values);

            //for (j = 0; j < values.Length; j++)
            {
                _timing[i] = float.Parse(values[0]);
                _lineNum[i] = int.Parse(values[1]);
                _objLineNum[i] = int.Parse(values[2]);
            }
            i++;
        }
    }

    float GetMusicTime()
    {
        return Time.time - _startTime;//音楽の時間を取得
    }
    public void MissTiming(int combo, int miss)
    {
        _combo = combo;//Missに衝突したときにコンボリセット
        _miss += miss;
    }

    public void JudgeMiss()
    {
        //judgeMiss = NoteScript.GetMissJudge();
        GoodUI.SetActive(false);
        PerfectUI.SetActive(false);
        BadUI.SetActive(false);
        MissUI.SetActive(true);
        isBad = false;
    }
 
    public void GoodTimingFunc(int num,string judgeName)
    {
        //ButtonController();
        //Debug.Log("Line:" + num + " " + jugdeName);
        //Debug.Log(GetMusicTime());

        //_score++;
        /*switch(judgeName)
        {
            case "Good":
                _score += 100;
                _combo+=1;
                break;

            case "Perfect":
                _score += 300;
                _combo+=1;
                break;

            case "Bad":
                //_score -=100;
                _combo = 0;
                break;
        }*/
        //判定の名前を見てそれぞれ処理を行う
        if (judgeName == "Good")//Goodの場合スコアを100加算し、コンボを1加算
        {            
            _score += good;
            _combo += 1;
            _good += 1;
            isBad = false;
            GoodUI.SetActive(true);
            PerfectUI.SetActive(false);
            BadUI.SetActive(false);
            MissUI.SetActive(false);
        }
        else if(judgeName == "Perfect")//Perfectの場合スコアを300加算し、コンボを1加算
        {
            _score += per;
            _combo += 1;
            _perfect += 1;
            isBad = false;
            GoodUI.SetActive(false);
            PerfectUI.SetActive(true);
            BadUI.SetActive(false);
            MissUI.SetActive(false);
        }
        else if(judgeName == "Bad")//Badの場合スコアを加算せずにコンボリセット
        {
            isBad = true;
            _combo = 0;
            _bad += 1;
            GoodUI.SetActive(false);
            PerfectUI.SetActive(false);
            BadUI.SetActive(true);
            MissUI.SetActive(false);
        }
        if(_comboCnt <= _combo)//マックスコンボを記録
        {
            _comboCnt = _combo;
        }
    }

    
}
