﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseScript : MonoBehaviour
{
    //ポーズ画面を開くボタン
    [SerializeField]
    private GameObject PauseButton;

    //ゲーム再開ボタン
    [SerializeField]
    private GameObject ReStartButton;

    //ポーズ背景
    [SerializeField]
    private GameObject PausePanel;

    //ポーズ時プレイボタン
    [SerializeField]
    private GameObject PlayButton;

    //ポーズ時リトライボタン
    [SerializeField]
    private GameObject RetryButton;

    //ポーズ時タイトルボタン
    [SerializeField]
    private GameObject TitleButton;

    private static string _nowScene;
    public void Update()
    {
        _nowScene = GameController.getSceneName();
        //停止中処理
        if (Time.timeScale == 0)
        {
            PauseButton.SetActive(false); //ポーズ画面非表示
        }

        //ゲーム進行中処理
        if(Time.timeScale == 1)
        {
            PauseButton.SetActive(true); //ポーズ画面表示
        }
    }

    //ポーズ
    public void StopGame()
    {
        Time.timeScale = 0f;//ゲーム一時停止
        PauseButton.SetActive(false);//ポーズボタンを非表示
        ReStartButton.SetActive(true);//リスタートボタン(ポーズボタン)を表示
        PausePanel.SetActive(true);//ポーズ背景表示

        //サウンド
        GameObject gc = GameObject.Find("GameController");//GameControllerクラスを取得
        gc.GetComponent<GameController>()._audioSource.Pause();//サウンド停止
    }

    //続きからゲームスタート
    public void ReStartGame()
    {
        PausePanel.SetActive(false);//ポーズ背景を消す
        ReStartButton.SetActive(false);//リスタートボタン(ポーズボタン)を非表示
        PauseButton.SetActive(true);//ポーズボタンを表示
        Time.timeScale = 1f;//通常プレイ

        //サウンド
        GameObject gc = GameObject.Find("GameController");//GameControllerクラスを取得
        gc.GetComponent<GameController>()._audioSource.UnPause();//サウンド再生
    }

    //初めからゲームスタート
    public void RetryGame()
    {
        SceneManager.LoadScene(_nowScene);//ゲームシーンリロード
        NoteScript.Initialize();
    }

    //タイトルに戻る
    public void Title()
    {
        SceneManager.LoadScene("Title");//タイトル
    }
}
