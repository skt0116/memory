﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameUtil
{

    public static KeyCode GetKeyCodeByLineNum(int lineNum)
    {
        //使用するキーを取得
        /*
        switch (lineNum)
        {
            case 0:
                return KeyCode.Mouse0;
            case 1:
                return KeyCode.Mouse0;
            case 2:
                return KeyCode.Mouse0;
            case 3:
                return KeyCode.Mouse0;
            default:
                return KeyCode.None;
        }
        */
        switch (lineNum)
        {
            case 0:
                return KeyCode.D;
            case 1:
                return KeyCode.F;
            case 2:
                return KeyCode.J;
            case 3:
                return KeyCode.K;
            default:
                return KeyCode.None;
        }
    }
}