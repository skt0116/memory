﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Load : MonoBehaviour
{
    public static int _loadScoreM;
    public static int _loadComboM;
    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.HasKey("MortalScore")) 
        { 
            _loadScoreM = PlayerPrefs.GetInt("MortalScore");
            _loadComboM = PlayerPrefs.GetInt("MortalCombo");
            Debug.Log("ロードしたMortalsの最大コンボ数："+_loadComboM);
            Debug.Log("ロードしたMortalsの最大スコア：" + _loadScoreM);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
