﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Save : MonoBehaviour
{
    public int _newHighScoreM;
    public int _newMaxComboM;
    public int _beforeScoreM;
    public int _beforeComboM;

    public static int _bestScoreM;
    public static int _bestComboM;

    public int _newHighScoreT;
    public int _newMaxComboT;
    public int _beforeScoreT;
    public int _beforeComboT;

    public static int _bestScoreT;
    public static int _bestComboT;

    // Start is called before the first frame update
    void Start()
    {
        if(PlayerPrefs.HasKey("MortalScore") && PlayerPrefs.HasKey("MortalCombo"))
        {
            _beforeScoreM = PlayerPrefs.GetInt("MortalScore");
            _beforeComboM = PlayerPrefs.GetInt("MortalCombo");

            _newHighScoreM = GameController.getscore();
            _newMaxComboM = GameController.getmaxCombo();

            if(_beforeScoreM > _newHighScoreM)
            {
                PlayerPrefs.SetInt("MortalScore",_beforeScoreM);
                PlayerPrefs.SetInt("MortalCombo", _beforeComboM);
            }
            else
            {
                PlayerPrefs.SetInt("MortalScore",_newHighScoreM);
                PlayerPrefs.SetInt("MortalCombo", _newMaxComboM);
            }
        }
        else
        {
            _newHighScoreM = GameController.getscore();
            _newMaxComboM = GameController.getmaxCombo();
            PlayerPrefs.SetInt("MortalScore", _newHighScoreM);
            PlayerPrefs.SetInt("MortalCombo", _newMaxComboM);
        }

        if (PlayerPrefs.HasKey("TurnScore") && PlayerPrefs.HasKey("TurnCombo"))
        {
            _beforeScoreT = PlayerPrefs.GetInt("TurnScore");
            _beforeComboT = PlayerPrefs.GetInt("TurnCombo");

            _newHighScoreT = GameController.getscore();
            _newMaxComboT = GameController.getmaxCombo();

            if (_beforeScoreT > _newHighScoreT)
            {
                PlayerPrefs.SetInt("TurnScore", _beforeScoreT);
                PlayerPrefs.SetInt("TurnCombo", _beforeComboT);
            }
            else
            {
                PlayerPrefs.SetInt("TurnScore", _newHighScoreT);
                PlayerPrefs.SetInt("TurnCombo", _newMaxComboT);
            }
        }
        else
        {
            _newHighScoreT = GameController.getscore();
            _newMaxComboT = GameController.getmaxCombo();
            PlayerPrefs.SetInt("TurnScore", _newHighScoreT);
            PlayerPrefs.SetInt("TurnCombo", _newMaxComboT);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            _beforeComboM = 0;
            _beforeScoreM = 0;
            _bestComboM = 0;
            _bestScoreM = 0;
            _newHighScoreM = 0;
            _newMaxComboM = 0;
            PlayerPrefs.SetInt("MortalScore", _newHighScoreM);
            PlayerPrefs.SetInt("MortalCombo", _newMaxComboM);

            _beforeComboT = 0;
            _beforeScoreT = 0;
            _bestComboT = 0;
            _bestScoreT = 0;
            _newHighScoreT = 0;
            _newMaxComboT = 0;
            PlayerPrefs.SetInt("TurnScore", _newHighScoreT);
            PlayerPrefs.SetInt("TurnCombo", _newMaxComboT);
        }
    }
}
